
(in-package :cl-user)

(defpackage :cle-scraping
  (:nicknames :scraping) 
  (:use :common-lisp)
  (:import-from :html5-parser :parse-html5)
  (:export :parse-html5-stream
           :parse-html5-file))

(in-package :cle-scraping)

(defun parse-html5-stream (stream &key (encoding :utf-8) (dom :cxml))
  (parse-html5 stream :strictp nil :encoding encoding :dom dom))

(defun parse-html5-file (path &key (encoding :utf-8) (dom :cxml))
  (with-open-file (in path 
                      :direction :input
                      :element-type '(unsigned-byte 8))
    (parse-html5-stream in :encoding encoding :dom dom)))

