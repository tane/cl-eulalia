(in-package :cl-user)

(defpackage #:cle-scraping-asd
  (:use :common-lisp :asdf))

(in-package :cle-scraping-asd)

(defsystem :cle-scraping
  :name "cle-scraping"
  :version "0.0.1"
  :maintainer "Benjamin Graf"
  :author "Benjamin Graf"
  :license "BSD"
  :description "Loads systems used for web scraping and provides utility functions."
  :serial t
  :components ((:file "scraping"))
  :depends-on (:cl-html5-parser-cxml
               :css-selectors))
