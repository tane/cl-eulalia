(defpackage #:cle-appdirs-asd
  (:use :cl :asdf))

(in-package :cle-appdirs-asd)

(defsystem :cle-appdirs
  :name "cle-appdirs"
  :version "0.0.1"
  :maintainer "Benjamin Graf"
  :author "Benjamin Graf"
  :license "BSD"
  :description "Yields Linux application directories."
  :serial t
  :components ((:file "appdirs"))
  :depends-on (:cl-fad))
