(in-package :cl-user)

(defpackage :cle-appdirs
  (:nicknames :appdirs)
  (:use :cl)
  (:import-from :fad :merge-pathnames-as-directory)
  (:export :*home*
           :*app*
           :user-data-dir
           :user-cache-dir
           :user-config-dir
           :user-log-dir))

(in-package :cle-appdirs)

(defun env-homedir-pathname () 
  (let ((homedir-str (user-homedir-pathname)))
    (if (null homedir-str)
      (error "Could not lookup home directory.")
      (truename (pathname homedir-str)))))

(defparameter *home* (user-homedir-pathname))

(defparameter *app* nil)

;; site-data-dir = /usr/local/share/<app>/

(defun appname-p (name)
  (and (stringp name) (< 0 (length name))))

(defun appname (name)
  (if (appname-p name)
    name
    (error "illegal app name")))

(defun app-pathname (name)
  (pathname (format nil "~a/" name)))

(defun user-dir (dirs app)
  (merge-pathnames-as-directory *home* dirs (app-pathname app) ))

(defun user-data-dir (&optional (app *app*))
  "Returns the user-data pathname for the given `app`. This is usually ~/.local/share/<app>/"
  (user-dir #p".local/share/" (appname app)))
            
(defun user-cache-dir (&optional (app *app*))
  "Returns the user-cache pathname for the given `app`. This is usually ~/.cache/<app>/"
  (user-dir #p".cache/" (appname app)))

(defun user-config-dir (&optional (app *app*))
  "Returns the user-config pathname for the given `app`. This is usually ~/.config/<app>/"
  (user-dir #p".config/" (appname app)))

(defun user-log-dir (&optional (app *app*))
  "Returns the user-log pathname for the given `app`. This is usually ~/.cache/<app>/log/"
  (merge-pathnames-as-directory (user-cache-dir app) #p"log/"))
