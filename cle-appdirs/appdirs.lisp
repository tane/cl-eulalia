(in-package :cl-user)

(defpackage :cle-appdirs
  (:nicknames :appdirs)
  (:use :cl)
  (:import-from :fad :merge-pathnames-as-directory)
  (:export :*app*
           :data-home
           :cache-home
           :config-home
           :log-home
           :make-path-as-directory
           :make-path-as-file
           :make-path-as-directory-fmt
           :make-path-as-file-fmt))

(in-package :cle-appdirs)

(defparameter *app* nil)

(defmacro apply-if (form f)
  (let ((var-name (gensym)))
    `(let ((,var-name ,form))
       (when ,var-name (,f ,var-name)))))

(defun get-env-path (name)
  (apply-if 
    (sb-ext:posix-getenv name) 
    cl-fad:pathname-as-directory))

(defun home-relative-dir (suffix)
  (make-path-as-directory (user-homedir-pathname) suffix))

(defun var-or-home-relative (varname suffix appname)
  (make-path-as-directory
    (or (get-env-path varname)
      (home-relative-dir suffix))
    appname))

(defun data-home (&optional (name *app*))
  (var-or-home-relative "XDG_DATA_HOME"
                        ".local/share/"
                        name))

(defun cache-home (&optional (name *app*))
  (var-or-home-relative "XDG_CACHE_HOME"
                        ".cache/"
                        name))

(defun config-home (&optional (name *app*))
  (var-or-home-relative "XDG_CONFIG_HOME"
                        ".config/"
                        name))

(defun log-home (&optional (name *app*))
  (make-path-as-directory 
    (cache-home name)
    "log/"))

(defun make-path-as-directory (&rest paths)
  (apply #'cl-fad:merge-pathnames-as-directory
         (mapcar #'cl-fad:pathname-as-directory paths)))

(defun make-path-as-file (&rest paths)
  (cl-fad:pathname-as-file
    (apply #'make-path-as-directory paths)))

(defun make-path-as-directory-fmt (&rest paths)
  (apply #'make-path-as-directory
         (mapcar #'(lambda (part) (format nil "~a" part)) paths)))

(defun make-path-as-file-fmt (&rest paths)
  (cl-fad:pathname-as-file
    (apply #'make-path-as-directory-fmt paths)))

